package br.com.template.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.template.entity.UserExample;
import br.com.template.entity.UserExampleEntity;
import br.com.template.parse.UserExampleEntityParaUserExampleParse;
import br.com.template.parse.UserExampleParaUserExampleEntityParse;
import br.com.template.port.UserRepositoryAdapter;
//DATA BASE ELSE CONFIG START
import br.com.template.repository.UserExampleEntityInMemoryRepository;
//DATA BASE ELSE CONFIG END
//DATA BASE CONFIG START
import br.com.template.repository.UserExampleEntityRepository;
//DATA BASE CONFIG END

@Component
public class UserExamplePersistence implements UserRepositoryAdapter {

	// DATA BASE CONFIG START
	@Autowired
	private UserExampleEntityRepository userExampleEntityRepository;
	// DATA BASE CONFIG END
	// DATA BASE ELSE CONFIG START
	@Autowired
	private UserExampleEntityInMemoryRepository userExampleEntityInMemoryRepository;
	// DATA BASE ELSE CONFIG END

	@Autowired
	private UserExampleEntityParaUserExampleParse userExampleEntityParaUserExampleParse;

	@Autowired
	private UserExampleParaUserExampleEntityParse userExampleParaUserExampleEntityParse;

	@Override
	public Optional<UserExample> findById(String id) {
		Optional<UserExampleEntity> userExampleEntity;
		// DATA BASE CONFIG START
		userExampleEntity = userExampleEntityRepository.findById(id);
		// DATA BASE CONFIG END
		// DATA BASE ELSE CONFIG START
		userExampleEntity = userExampleEntityInMemoryRepository.findById(id);
		// DATA BASE ELSE CONFIG END

		if (!userExampleEntity.isPresent()) {
			return Optional.ofNullable(null);
		}

		return Optional.of(userExampleEntityParaUserExampleParse.parse(userExampleEntity.get()));
	}

	@Override
	public List<UserExample> findAllUsers() {
		List<UserExampleEntity> userExampleEntityList;
		// DATA BASE CONFIG START
		userExampleEntityList = userExampleEntityRepository.findAll();
		// DATA BASE CONFIG END
		// DATA BASE ELSE CONFIG START
		userExampleEntityList = userExampleEntityInMemoryRepository.findAll();
		// DATA BASE ELSE CONFIG END

		return userExampleEntityParaUserExampleParse.parseList(userExampleEntityList);
	}

	@Override
	public void create(UserExample userExample) {
		// DATA BASE CONFIG START
		userExampleEntityRepository.save(userExampleParaUserExampleEntityParse.parse(userExample));
		// DATA BASE CONFIG END
		// DATA BASE ELSE CONFIG START
		userExampleEntityInMemoryRepository.save(userExampleParaUserExampleEntityParse.parse(userExample));
		// DATA BASE ELSE CONFIG END
	}

	@Override
	public Optional<UserExample> findByEmail(String email) {
		List<UserExampleEntity> userExampleEntityList;
		// DATA BASE CONFIG START
		userExampleEntityList = userExampleEntityRepository.findAllByEmail(email);
		// DATA BASE CONFIG END
		// DATA BASE ELSE CONFIG START
		userExampleEntityList = userExampleEntityInMemoryRepository.findAllByEmail(email);
		// DATA BASE ELSE CONFIG END

		if (userExampleEntityList.isEmpty()) {
			return Optional.ofNullable(null);
		} else {
			return Optional.of(userExampleEntityParaUserExampleParse.parse(userExampleEntityList.get(0)));
		}
	}

}
