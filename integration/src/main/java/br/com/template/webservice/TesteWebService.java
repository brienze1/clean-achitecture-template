package br.com.template.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.template.port.TesteWebServiceAdapter;

@Component
public class TesteWebService implements TesteWebServiceAdapter {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${server.port}")
	private String serverPort;

	public String test() {
		return restTemplate.getForEntity("http://localhost:" + serverPort + "/testeconexao", String.class).getBody();
	}
	
}
