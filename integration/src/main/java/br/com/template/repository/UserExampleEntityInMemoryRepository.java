package br.com.template.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import br.com.template.entity.UserExampleEntity;

@Component
public class UserExampleEntityInMemoryRepository {
	
	private static List<UserExampleEntity> userExampleEntityList;
	
	@PostConstruct
	public void init() {
		userExampleEntityList = new ArrayList<>();
	}

	public List<UserExampleEntity> findAll() {
		return userExampleEntityList;
	}

	public void save(UserExampleEntity userExampleEntity) {
		userExampleEntityList.add(userExampleEntity);	
	}

	public Optional<UserExampleEntity> findById(String id) {
		for (UserExampleEntity userExampleEntity : userExampleEntityList) {
			if(userExampleEntity.getId().equals(id)) {
				return Optional.of(userExampleEntity);
			}
		}
		return Optional.ofNullable(null);
	}

	public List<UserExampleEntity> findAllByEmail(String email) {
		List<UserExampleEntity> userByEmailList = new ArrayList<>();
		for (UserExampleEntity userExampleEntity : userExampleEntityList) {
			if(userExampleEntity.getEmail().equals(email)) {
				userByEmailList.add(userExampleEntity);
			}
		}
		return userByEmailList;
	}
	
}
