package br.com.template.utils;

import java.util.UUID;

import org.springframework.stereotype.Component;

import br.com.template.port.IdGeneratorAdapter;

@Component
public class UuidGenerator implements IdGeneratorAdapter {

	@Override
	public String generate() {
		return UUID.randomUUID().toString();
	}
}
