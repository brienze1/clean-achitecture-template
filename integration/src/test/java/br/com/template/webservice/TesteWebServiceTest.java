package br.com.template.webservice;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
public class TesteWebServiceTest {

	@InjectMocks
	private TesteWebService testeWebService;
	
	@Mock
	private RestTemplate restTemplate;
	
	@Before
	public void init() {
		ReflectionTestUtils.setField(testeWebService, "serverPort", "8080");
		
	}
	
	@Test
	public void test() {
		Mockito.when(restTemplate.getForEntity("http://localhost:" + "8080" + "/testeconexao", String.class)).thenReturn(new ResponseEntity<String>("teste", HttpStatus.OK));
		
		String response = testeWebService.test();
		
		Assert.assertEquals("teste", response);
	}
	
}
