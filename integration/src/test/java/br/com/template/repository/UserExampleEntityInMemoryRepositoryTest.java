package br.com.template.repository;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.template.entity.UserExampleEntity;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserExampleEntityInMemoryRepositoryTest {

	@InjectMocks
	private UserExampleEntityInMemoryRepository userExampleEntityInMemoryRepository;
	
	private UserExampleEntity userExampleEntity;
	private UserExampleEntity userExampleEntity2;
	
	@Before
	public void init() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		userExampleEntityInMemoryRepository.init();
		
		userExampleEntity = new UserExampleEntity();
		userExampleEntity.setEmail("jhonmartson@email.com");
		userExampleEntity.setFirstName("Jhon");
		userExampleEntity.setLastName("Marston");
		userExampleEntity.setId(UUID.randomUUID().toString());
		userExampleEntity.setPassword(UUID.randomUUID().toString());
		
		userExampleEntity2 = new UserExampleEntity();
		userExampleEntity2.setEmail("jhonmartson2@email.com");
		userExampleEntity2.setFirstName("Jhon");
		userExampleEntity2.setLastName("Marston");
		userExampleEntity2.setId(UUID.randomUUID().toString());
		userExampleEntity2.setPassword(UUID.randomUUID().toString());
		
		userExampleEntityInMemoryRepository.save(userExampleEntity2);
	}
	
	@Test
	public void testSave() {
		userExampleEntityInMemoryRepository.save(userExampleEntity);
		
		List<UserExampleEntity> response = userExampleEntityInMemoryRepository.findAll();
		
		Assert.assertTrue(response.size() == 2);
		Assert.assertEquals(userExampleEntity2.getEmail(), response.get(0).getEmail());
		Assert.assertEquals(userExampleEntity2.getFirstName(), response.get(0).getFirstName());
		Assert.assertEquals(userExampleEntity2.getId(), response.get(0).getId());
		Assert.assertEquals(userExampleEntity2.getLastName(), response.get(0).getLastName());
		Assert.assertEquals(userExampleEntity2.getPassword(), response.get(0).getPassword());
	}

	@Test
	public void testFindById() {
		userExampleEntityInMemoryRepository.save(userExampleEntity);
		
		Optional<UserExampleEntity> response = userExampleEntityInMemoryRepository.findById(userExampleEntity.getId());
		
		Assert.assertTrue(response.isPresent());
		Assert.assertEquals(userExampleEntity.getEmail(), response.get().getEmail());
		Assert.assertEquals(userExampleEntity.getFirstName(), response.get().getFirstName());
		Assert.assertEquals(userExampleEntity.getId(), response.get().getId());
		Assert.assertEquals(userExampleEntity.getLastName(), response.get().getLastName());
		Assert.assertEquals(userExampleEntity.getPassword(), response.get().getPassword());
	}

	@Test
	public void testFindByIdNotFound() {
		Optional<UserExampleEntity> response = userExampleEntityInMemoryRepository.findById(userExampleEntity.getId());
		
		Assert.assertTrue(!response.isPresent());
	}

	@Test
	public void testFindByEmail() {
		userExampleEntityInMemoryRepository.save(userExampleEntity);
		
		List<UserExampleEntity> response = userExampleEntityInMemoryRepository.findAllByEmail(userExampleEntity.getEmail());
		
		Assert.assertTrue(response.size() == 1);
		Assert.assertEquals(userExampleEntity.getEmail(), response.get(0).getEmail());
		Assert.assertEquals(userExampleEntity.getFirstName(), response.get(0).getFirstName());
		Assert.assertEquals(userExampleEntity.getId(), response.get(0).getId());
		Assert.assertEquals(userExampleEntity.getLastName(), response.get(0).getLastName());
		Assert.assertEquals(userExampleEntity.getPassword(), response.get(0).getPassword());
	}
	
	@Test
	public void testFindByEmailNotFound() {
		List<UserExampleEntity> response = userExampleEntityInMemoryRepository.findAllByEmail(userExampleEntity.getEmail());
		
		Assert.assertTrue(response.isEmpty());
	}
	
}
