Template de projeto Clean Architecture para utilizacao em projetos futuros.

Separado em 4 camadas:
   *  Application -> Configuracao dos beans do projeto e properties
   *  Delivery -> Camada exposta da API, dtos e controlers...
   *  Domain -> Camada de servico, regras de negocio etc...
   *  Integration -> Camada de integracao da API com banco de dados, chamadas Rest etc...
  

Proximos passos:

*  Adicionar testes unitarios para as camadas
*  Adicionar teste integrado com cucumber


Feito por Luis Brienze. 
Linkedin: https://www.linkedin.com/in/luisbrienze/