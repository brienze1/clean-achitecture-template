package br.com.template.parse;

import org.springframework.stereotype.Component;

import br.com.template.dto.UserExampleDTO;
import br.com.template.entity.UserExample;

@Component
public class UserExampleDTOParaUserExampleParse {
	
	public UserExample parse(UserExampleDTO userExampleDTO) {
		UserExample userExample = new UserExample();
		
		if(userExampleDTO != null) {
			userExample.setEmail(userExampleDTO.getEmail());
			userExample.setFirstName(userExampleDTO.getFirstName());
			userExample.setLastName(userExampleDTO.getLastName());
			userExample.setPassword(userExampleDTO.getPassword());
		}
		
		return userExample;
	}

}
