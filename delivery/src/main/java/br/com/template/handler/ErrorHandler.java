package br.com.template.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import br.com.template.exception.UserAlreadyExistsExceptionExample;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler({UserAlreadyExistsExceptionExample.class})
	public ResponseEntity<Object> errorHandler(Exception ex) {
		throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
	}
	
}
