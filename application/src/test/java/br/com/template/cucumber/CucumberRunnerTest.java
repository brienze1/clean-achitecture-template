package br.com.template.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features", 
		glue = {"br.com.template.cucumber.steps.busca", "br.com.template.cucumber.steps.cadastro"},
		plugin = "pretty")
public class CucumberRunnerTest {

}
