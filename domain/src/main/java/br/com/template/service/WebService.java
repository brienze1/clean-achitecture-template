package br.com.template.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.template.port.TesteWebServiceAdapter;
import br.com.template.port.WebServiceAdapter;

@Component
public class WebService implements WebServiceAdapter {

	@Autowired
	private TesteWebServiceAdapter testeWebService;
	
	public String testRestTemplate() {
		return testeWebService.test();
	}
	
}
