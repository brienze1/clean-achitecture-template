package br.com.template.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.template.entity.UserExample;
import br.com.template.port.IdGeneratorAdapter;
import br.com.template.port.PasswordEncoderAdapter;
import br.com.template.port.UserRepositoryAdapter;
import br.com.template.port.UserServiceAdapter;
import br.com.template.rules.UserValidatorExample;

@Component
public class UserService implements UserServiceAdapter {

	@Autowired
	private UserValidatorExample userValidatorExample;
	
	@Autowired
	private UserRepositoryAdapter repository;
	
	@Autowired
	private PasswordEncoderAdapter passwordEncoder;
	
	@Autowired
	private IdGeneratorAdapter idGenerator;
	
	public String create(UserExample user) {
		userValidatorExample.validateCreateUser(user);
		
		user.setId(idGenerator.generate());
		user.setPassword(passwordEncoder.encode(user.getEmail()+user.getPassword()));
		
		repository.create(user);
		
		return user.getId();
	}
	
	public Optional<UserExample> findById(final String id){
		return repository.findById(id);
	}
	
	public List<UserExample> findAllUsers(){
		return repository.findAllUsers();
	}
	
	public Optional<UserExample> findByEmail(String email){
		return repository.findByEmail(email);
	}
	
}
