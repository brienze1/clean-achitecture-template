package br.com.template.rules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.template.entity.UserExample;
import br.com.template.exception.UserAlreadyExistsExceptionExample;
import br.com.template.port.UserRepositoryAdapter;

@Component
public class UserValidatorExample {
	
	@Autowired
	private UserRepositoryAdapter repository;
	
	public void validateCreateUser(UserExample user) {
		if (repository.findByEmail(user.getEmail()).isPresent()) {
			throw new UserAlreadyExistsExceptionExample("User " + user.getEmail() + " already exists");
		}
	}

}
