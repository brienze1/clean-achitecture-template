package br.com.template.port;

import org.springframework.stereotype.Component;

@Component
public interface PasswordEncoderAdapter {

	String encode(String string);

}
