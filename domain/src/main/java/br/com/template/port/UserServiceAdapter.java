package br.com.template.port;

import java.util.List;
import java.util.Optional;

import br.com.template.entity.UserExample;
import br.com.template.exception.UserAlreadyExistsExceptionExample;

public interface UserServiceAdapter {

	public String create(UserExample user) throws UserAlreadyExistsExceptionExample;
	
	public Optional<UserExample> findById(final String id);
	
	public List<UserExample> findAllUsers();
	
	public Optional<UserExample> findByEmail(String email);
}
