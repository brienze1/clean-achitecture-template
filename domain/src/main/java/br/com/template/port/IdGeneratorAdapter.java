package br.com.template.port;

import org.springframework.stereotype.Component;

@Component
public interface IdGeneratorAdapter {

	String generate();

}
