package br.com.template.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.template.port.TesteWebServiceAdapter;

@RunWith(SpringRunner.class)
public class WebServiceTest {

	@InjectMocks
	private WebService webService;
	
	@Mock
	private TesteWebServiceAdapter testeWebService;
	
	@Test
	public void test() {
		Mockito.when(testeWebService.test()).thenReturn("teste");
		
		String response = webService.testRestTemplate();
		
		Assert.assertEquals("teste", response);
	}
	
}
